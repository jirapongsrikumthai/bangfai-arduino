

//=========================== include ================================
#include <RH_E32.h>//LoRa
#include "SoftwareSerial.h"
#include <Wire.h>//BME280&Gyro
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <TinyGPS++.h> //GPS
#include <L3G.h> //Gyro
#include <LSM303.h>
#include <math.h>
#include <avr/wdt.h>
//=========================== Parametor ================================
uint8_t m0_pin = 4;//LoRa
uint8_t m1_pin = 5;
uint8_t aux_pin = 2;
uint8_t tx_pin = 6;
uint8_t rx_pin = 7;
#define SEALEVELPRESSURE_HPA (1013.25)//BME280
Adafruit_BME280 bme; 
L3G gyro;//Gyro

//=========================== Parameter  Globle================================
unsigned int alt_int=0,temp_int=0, humid_int=0, volt_int=0, psure_int=0,gX = 0,gY=0,gZ=0;
long intervalAcc = 200; 
long intervalBME280 = 350; 
long interval = 1000; 

unsigned long previousMillisAcc = 0; 
unsigned long previousMillisBME280 = 0; 
unsigned long previousMillis = 0; 

void bmeGetValue();
void GyroGetValue();
//=========================== LoRa ================================
SoftwareSerial LoRa(tx_pin, rx_pin);
RH_E32  driver(&LoRa, m0_pin, m1_pin, aux_pin);
//=========================== GPS================================

static const uint32_t GPSBaud = 9600;
static const uint32_t serialBaud = 9600;
static const uint32_t loraBaud = 9600;


static const int RXPin = 4, TXPin = 3;

LSM303 compass;
TinyGPSPlus gps;

double Long, Lat, GPS_Alt;
double ax, ay, az, mx, my, mz, roll, pitch, yaw, mag_x, mag_y, rollRaw, pitchRaw, yawRaw, rollSpeed, pitchSpeed, yawSpeed, gmag_x, gmag_y, gx, gy, gz; //edit
float process_noise = 1;
float sensor_noise = 3;

uint8_t buff[30];
char gps_lat[12] = "0";
char gps_lng[12] = "0";
char presure[8] = "0";
char volte[4] = "0.0";

float ss = 0.0;
int gps_hr = 0, gps_min = 0,gps_sec = 0;
float myFloat = 99.9911;
void displayInfo();


char aX[7] ="0";
char aY[7] ="0";
char aZ[7] ="0";
float ss = 0.0;
int gps_hr = 0, gps_min = 0,gps_sec = 0;
float myFloat = 99.9911;
void displayInfo();

void setup() 
{
    pinMode(13,OUTPUT);
    digitalWrite(13,HIGH);
    delay(1000);
    digitalWrite(13,LOW);
    Serial.begin(9600);
    LoRa.begin(9600); 
    Wire.begin();
    compass.init();
    compass.enableDefault();
    gyro.init();
    gyro.enableDefault();
    bme.begin();
    buff[0] = 0x06;
    wdt_enable(WDTO_4S);
}

void loop(){ 
    //GPS
    while (Serial.available() > 0){
      gps.encode(Serial.read());
    }
    //Gyro
    unsigned long currentMillisAcc = millis(); 
    if(currentMillisAcc - previousMillisAcc > intervalAcc){//200
      previousMillisAcc = currentMillisAcc;
      getGyroValue();
    }
    //bme280
    unsigned long currentMillisBME80 = millis(); 
    if(currentMillisBME80 - previousMillisBME280 > intervalBME280){//350
      previousMillisBME280 = currentMillisBME80;
      bmeGetValue();
    }
    //Lora
    unsigned long currentMillis = millis(); 
    if(currentMillis - previousMillis > interval){//1000
       previousMillis = currentMillis;
       digitalWrite(13,HIGH);
       gpsGetValue();
       getVoltage();       
       sendPacketData();
       digitalWrite(13,LOW);
    }
    wdt_reset();
}
//============================================= Packet Lora =================================================
void sendPacketData(){
  buff[sizeof(buff)-1] = 0x0A;   
  //uint8_t buff1[35] = {0x01,0x0D,0x12,0x5C,0x43,0x64,0x61,0x36,0x48,0xFE,0xFE,0x23,0xFE,0xFE,0x4B,0x11,0x23,0x21,0x16,0x21,0x21,0x2C,0xDE,0x23,0x21,0x16,0x21,0x21,0x2C,0xDE,0x0A};
  driver.send(buff, sizeof(buff));
  driver.waitPacketSent();
}
//============================================= Sensor =====================================================

void bmeGetValue() {
    //int presour = (int)(bme.readPressure() / 100.0F);
  //        appendTobuff(String((int)bme.readAltitude(SEALEVELPRESSURE_HPA)), 9, 0, 2, 2, 4, true); // alt 09
    appendTobuff(String((int)bme.readTemperature()), 11, 0, 2, 2, 2, true);                 // temp  //11
    //double presure = (bme.readPressure() / 100.0F);
    
    //appendTobuff(String(presour), 12, 0, 2, 2, 4, true);         // pressure  //15-16
    dtostrf((float)(bme.readPressure() / 100.0F), 2, 2,presure); 
    splitStr(presure, 12, 0, 2, 2, 4, 2);//latitude   //12-14
    Wire.endTransmission(); 
    
}

void gpsGetValue(){
  if (gps.location.isValid())
  {
    dtostrf((float)gps.location.lat(), 2, 6,gps_lat);    
    dtostrf((float)gps.location.lng(), 2, 6,gps_lng); 
    splitStr(gps_lat, 1, 0, 2, 2, 3, 6);//latitude   //01-04
    //splitStr(gps_lng, 5, 0, 2, 2, 3, 6);//longtitude //05-08
    //char gps_t[12] = "13.452845";
    //splitStr(gps_t, 1, 0, 2, 2, 3, 6);//latitude   //01-04
    splitStr(gps_lng, 5, 0, 2, 2, 3, 6);//longtitude //05-08
  }
  if (gps.time.isValid())
  {
    gps_hr = (int)gps.time.hour();
    gps_min = (int)gps.time.minute();
    gps_sec = (int)gps.time.second();
    appendTobuff(String((int)gps.time.hour()), 16, 0, 2, 2, 2, true); //HH //16
    appendTobuff(String((int)gps.time.minute()), 17, 0, 2, 2, 2, true); //mm //17
    appendTobuff(String((int)gps.time.second()), 18, 0, 2, 2, 2, true); //ss //18
  }
  appendTobuff(String((int)gps.altitude.meters()), 9, 0, 2, 2, 4, true); //alt //9-10
}

char* deblank(char* input)                                         
{
    int i,j;
    char *output=input;
    for (i = 0, j = 0; i<strlen(input); i++,j++)          
    {
        if (input[i]!=' ')                           
            output[j]=input[i];                     
        else
            j--;                                     
    }
    output[j]=0;
    return output;
}
void getGyroValue(){
  
    compass.read();
    ax = (0.122 * (signed int)compass.a.x)/1000;
    ay = (0.122 * (signed int)compass.a.y)/1000;
    az = (0.122 * (signed int)compass.a.z)/1000;
      
    int negativeAcc = 0;
    if(ax < 0){
      ax=ax * (-1);
      negativeAcc += 200;
    }else{
      negativeAcc += 100;
    }
    if(ay < 0){
      ay=ay * (-1);
      negativeAcc += 20;
    }else{
      negativeAcc += 10;
    }
    if(az <0){
      az=az * (-1);
      negativeAcc += 2;
    }else{
      negativeAcc += 1;
    }
  
    dtostrf(ax, 2, 2,aX); 
    dtostrf(ay, 2, 2,aY);      
    dtostrf(az, 2, 2,aZ);    
    splitStr(aX, 19, 0, 2, 2, 2, 2);//aX 19-20
    splitStr(aY, 21, 0, 2, 2, 2, 2);//aY  21-22
    splitStr(aZ, 23, 0, 2, 2, 2, 2);//aZ //23-24
    buff[25] = (uint8_t)(negativeAcc); //25
}

void getVoltage(){
    int sensorValue = analogRead(A0);
    //int volte = 75;
    int volte = (int)((sensorValue * (5.00 / 1023.00) * 2)*10);
    appendTobuff(String(volte), 15, 0, 0, 2, 2, true); // Volte //15
}
 //splitStr(gps_lat, 1, 0, 2, 2, 6);//latitude   //03-06
//============================================ Tools ========================================================
void splitStr(char*f,int buffStart,int subStart,int subEnd,int subCount,int lenInt, int lenDec){
    String str;
    char *p = f;
    int i = 0;
    while (str = strtok (p, ".")){
       if(i == 0)
          if(lenInt >= 4){
            appendTobuff(str,buffStart, subStart,subEnd,subCount,lenInt, true);
          }else{
            buff[buffStart] = (uint8_t)(str.toInt());
          }
       else if(i == 1){
          if(lenInt >= 4){
            appendTobuff(str,buffStart + 2, subStart, subEnd, subCount, lenDec, false);
          }else{
            appendTobuff(str,buffStart + 1, subStart, subEnd, subCount, lenDec, false);
          }
       } 
       i++;
       p = NULL;
    }
}

void appendTobuff(String str, int buffStart, int startIndex, int endIndex, int countIndex, int len, bool Isleft){
    if(str[0] == '-')
      str = "0";
    if(Isleft){
      String zero;
      for(int i=str.length(); i<len; i++) zero+='0';
      str = zero + str;
    }else
      for(int i=str.length(); i<len; i++) str+='0';
    int cont = len/countIndex;
    for(int i=0 ;i<cont;i++){
      if((uint8_t)(str.substring(startIndex,endIndex).toInt()) == 0x0A)
        buff[buffStart] = 0xFE;
      else
        buff[buffStart] = (uint8_t)(str.substring(startIndex,endIndex).toInt());
      buffStart++;
      startIndex+=countIndex;
      endIndex+=countIndex;
    }
}

float Kalman(float rawdata, float prev_est) {  // Kalman Filter
    float a_priori_est, a_post_est, a_priori_var, a_post_var, kalman_gain;
  
    a_priori_est = prev_est;
    a_priori_var = process_noise;
  
    kalman_gain = a_priori_var / (a_priori_var + sensor_noise);
    a_post_est = a_priori_est + kalman_gain * (rawdata - a_priori_est);
    a_post_var = (1 - kalman_gain) * a_priori_var;
    return a_post_est;
} 
